package ru.pyshinskiy.tm.entity;

import ru.pyshinskiy.tm.role.Role;

public final class User extends AbstractEntity {
    private String password;

    private String login;

    private Role role;

    public User() {
    }

    public User(final Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }
}

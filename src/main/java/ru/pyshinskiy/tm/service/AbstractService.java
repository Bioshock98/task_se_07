package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.api.service.Service;
import ru.pyshinskiy.tm.entity.AbstractEntity;
import ru.pyshinskiy.tm.repository.AbstractRepository;

import java.util.List;

public class AbstractService<T extends AbstractEntity> implements Service<T> {

    protected AbstractRepository<T> abstractRepository;

    public AbstractService(final AbstractRepository<T> abstractRepository) {
        this.abstractRepository = abstractRepository;
    }

    @Override
    public T findOne(final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return abstractRepository.findOne(id);
    }

    @Override
    public List<T> findAll() {
        return abstractRepository.findAll();
    }

    @Override
    public T persist(final T t) {
        if(t == null) return null;
        return abstractRepository.persist(t);
    }

    @Override
    public T merge(final T t) throws Exception {
        if(t == null) return null;
        return abstractRepository.merge(t);
    }

    @Override
    public T remove(final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        return abstractRepository.remove(id);
    }

    @Override
    public void removeAll() {
        abstractRepository.removeAll();
    }

    @Override
    public String getIdByNumber(final int number) {
        return abstractRepository.getIdByNumber(number);
    }
}

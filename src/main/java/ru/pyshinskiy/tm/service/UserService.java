package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.AbstractRepository;

public class UserService extends AbstractService<User> implements IUserService {
    public UserService(final AbstractRepository<User> abstractRepository) {
        super(abstractRepository);
    }
}

package ru.pyshinskiy.tm.service;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.AbstractRepository;
import ru.pyshinskiy.tm.repository.ProjectRepository;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {
    
    private ProjectRepository projectRepository = (ProjectRepository) abstractRepository;
    
    public ProjectService(final AbstractRepository<Project> projectRepository) {
        super(projectRepository);
    }

    @Override
    public Project findOne(final String userId, final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception();
        if(userId == null || userId.isEmpty()) throw new Exception();
        return projectRepository.findOne(userId, id);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if(userId == null) return null;
        return projectRepository.findAll(userId);
    }

    @Override
    public Project remove(final String userId, final String id) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        if(id == null || id.isEmpty()) throw new Exception();
        return projectRepository.remove(id);
    }

    @Override
    public void removeAll(final String userId) throws Exception {
        if(userId == null || userId.isEmpty()) throw new Exception();
        projectRepository.remove(userId);
    }
}

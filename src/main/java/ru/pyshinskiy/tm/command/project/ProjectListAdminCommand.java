package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "project_list_admin";
    }

    @Override
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        printProjects(serviceLocator.getProjectService().findAll());
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Project;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_create";
    }

    @Override
    public String description() {
        return "create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        final Project project = new Project(serviceLocator.getCurrentUser().getId());
        project.setName(terminalService.nextLine());
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(terminalService.nextLine()));
        serviceLocator.getProjectService().persist(project);
        System.out.println("[OK]");
    }
}

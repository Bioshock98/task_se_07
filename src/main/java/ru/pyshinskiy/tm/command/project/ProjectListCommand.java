package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project_list";
    }

    @Override
    public String description() {
        return "show all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        final IProjectService projectService = serviceLocator.getProjectService();
        printProjects(projectService.findAll(serviceLocator.getCurrentUser().getId()));
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.project;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectRemoveAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "project_remove_admin";
    }

    @Override
    public String description() {
        return "remove any selected project";
    }

    @Override
    public void execute() throws Exception {
        final IProjectService projectService = serviceLocator.getProjectService();
        final ITaskService taskService = serviceLocator.getTaskService();
        final User currentUser = serviceLocator.getCurrentUser();
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT'S ID");
        printProjects(projectService.findAll());
        final String projectId = projectService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        taskService.removeAllByProjectId(currentUser.getId(), projectId);
        projectService.remove(projectId);
        System.out.println("[PROJECT REMOVED]");
    }
}

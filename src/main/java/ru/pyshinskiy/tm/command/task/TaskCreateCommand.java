package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_create";
    }

    @Override
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER TASK NAME]");
        final Task task = new Task(serviceLocator.getCurrentUser().getId());
        task.setName(terminalService.nextLine());
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(terminalService.nextLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(terminalService.nextLine()));
        serviceLocator.getTaskService().persist(task);
        System.out.println("[OK]");
        serviceLocator.getTaskService().persist(task);
    }
}

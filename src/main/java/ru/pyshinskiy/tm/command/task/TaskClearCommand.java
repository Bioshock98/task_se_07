package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task_clear";
    }

    @Override
    public String description() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASK]");
        serviceLocator.getTaskService().removeAll(serviceLocator.getCurrentUser().getId());
        System.out.println("[ALL TASKS REMOVED]");
    }
}

package ru.pyshinskiy.tm.command.task;

import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTask;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskSelectAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "task_select_admin";
    }

    @Override
    public String description() {
        return "show any user selected task info";
    }

    @Override
    public void execute() throws Exception {
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        printTasks(taskService.findAll());
        final String taskId = taskService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
        final Task task = taskService.findOne(taskId);
        printTask(task);
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.about;

import com.jcabi.manifests.Manifests;
import ru.pyshinskiy.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "about";
    }

    @Override
    public String description() {
        return "print build number info";
    }

    @Override
    public void execute() throws Exception {
        String buildNumber = Manifests.read("buildNumber");
        String developer = Manifests.read("developer");
        System.out.println("build number: " + buildNumber + "\n" + "developer: " + developer);
    }
}

package ru.pyshinskiy.tm.command.user;

import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

import javax.swing.plaf.IconUIResource;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUsers;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "user_remove";
    }

    @Override
    public String description() {
        return "remove existing user";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator.getCurrentUser().getRole().equals(Role.USER)) {
            System.out.println("!UNAVAILABLE COMMAND!");
        }
        else {
            System.out.println("ENTER USER ID");
            printUsers(serviceLocator.getUserService().findAll());
            final IUserService userService = serviceLocator.getUserService();
            final String userId = userService.getIdByNumber(Integer.parseInt(terminalService.nextLine()) - 1);
            serviceLocator.getUserService().remove(userId);
            System.out.println("USER REMOVED");
        }
    }
}

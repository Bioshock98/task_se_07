package ru.pyshinskiy.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;

public final class UserEditCommand extends AbstractCommand {
    
    @Override
    public String command() {
        return "user_edit";
    }

    @Override
    public String description() {
        return "edit existing user";
    }

    @Override
    public void execute() throws Exception {
        final String currentUserPassword = serviceLocator.getCurrentUser().getPassword();
        System.out.println("[USER EDIT]");
        System.out.println("ENTER PASSWORD");
        while(currentUserPassword.equals(DigestUtils.md5Hex(terminalService.nextLine()))) {
            System.out.println("Incorrect password");
        }
        System.out.println("ENTER NEW USERNAME");
        final User user = new User();
        user.setId(serviceLocator.getCurrentUser().getId());
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER NEW PASSWORD");
        user.setPassword(terminalService.nextLine());
        serviceLocator.getUserService().merge(user);
        System.out.println("[OK]");
    }
}

package ru.pyshinskiy.tm.command.user;

import ru.pyshinskiy.tm.command.AbstractCommand;

public final class UserLogOutCommand extends AbstractCommand {
    @Override
    public String command() {
        return "user_log_out";
    }

    @Override
    public String description() {
        return "log out";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        serviceLocator.setCurrentUser(null);
        System.out.println("[OK]");
    }
}

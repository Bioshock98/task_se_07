package ru.pyshinskiy.tm.command.user;

import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.role.Role;

public final class UserCreateAdminCommand extends AbstractCommand {

    @Override
    public boolean isAllowed(final User user) {
        if(user == null) return false;
        return Role.ADMINISTRATOR.equals(user.getRole());
    }

    @Override
    public String command() {
        return "user_create_admin";
    }

    @Override
    public String description() {
        return "create a new user or administrator";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        final User user = new User();
        System.out.println("ENTER ROLE");
        user.setRole(Role.valueOf(terminalService.nextLine().toUpperCase()));
        System.out.println("ENTER USERNAME");
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER PASSWORD");
        user.setPassword(terminalService.nextLine());
        serviceLocator.getUserService().persist(user);
        System.out.println("[OK]");
    }
}

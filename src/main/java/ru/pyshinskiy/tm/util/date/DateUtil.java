package ru.pyshinskiy.tm.util.date;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static Date parseDateFromString(final String stringDate) {
        return dateFormatter.parse(stringDate, new ParsePosition(0));
    }

    public static String parseDateToString(final Date date) {
        return dateFormatter.format(date);
    }
}

package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.api.task.ITaskRepository;
import ru.pyshinskiy.tm.entity.Task;

import java.util.LinkedList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(final String userId) {
        final LinkedList<Task> userTasks = new LinkedList<>();
        for(final Task task : findAll()) {
            if(task.getUserId().equals(userId)) userTasks.add(task);
        }
        return userTasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final LinkedList<Task> projectTasks = new LinkedList<>();
        for(final Task task : findAll(userId)) {
            if(task.getProjectId().equals(projectId)) projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public Task findOne(final String userId, final String id) throws Exception {
        for(final Task task : findAll()) {
            if(task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    @Override
    public Task remove(final String userId, final String id) throws Exception {
        return entityMap.remove(id);
    }

    @Override
    public void removeAll(final String userId) throws Exception {
        for(final Task task : findAll()) {
            if(task.getUserId().equals(userId)) remove(task.getId());
        }
    }

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) throws Exception {
        for(final Task task : findAll(userId)) {
            if(task.getProjectId().equals(projectId)) remove(userId, task.getId());
        }
    }
}

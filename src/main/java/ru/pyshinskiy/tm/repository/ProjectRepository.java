package ru.pyshinskiy.tm.repository;

import ru.pyshinskiy.tm.api.project.IProjectRepository;
import ru.pyshinskiy.tm.entity.Project;

import java.util.LinkedList;
import java.util.List;


public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final String userId) {
        final LinkedList<Project> userProjects = new LinkedList<>();
        for(final Project project : findAll()) {
            if(project.getUserId().equals(userId)) userProjects.add(project);
        }
        return userProjects;
    }

    @Override
    public Project findOne(final String userId, final String id) throws Exception {
        for(final Project project : findAll()) {
            if(project.getUserId().equals(userId)) return project;
        }
        return null;
    }

    @Override
    public Project remove(final String userId, final String id) throws Exception {
        return entityMap.remove(id);
    }

    @Override
    public void removeAll(final String userId) throws Exception {
        for(final Project project : findAll()) {
            if(project.getUserId().equals(userId)) remove(project.getId());
        }
    }
}

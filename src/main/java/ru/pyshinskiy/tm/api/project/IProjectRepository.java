package ru.pyshinskiy.tm.api.project;

import ru.pyshinskiy.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(final String userId);

    Project findOne(final String id) throws Exception;

    Project persist(final Project project);

    Project merge(final Project merge) throws Exception;

    Project findOne(final String userId, String id) throws Exception;

    Project remove(final String id) throws Exception;

    Project remove(final String userId, final String id) throws Exception;

    void removeAll(final String userId) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);

}

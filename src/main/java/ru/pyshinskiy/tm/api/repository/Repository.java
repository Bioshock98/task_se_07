package ru.pyshinskiy.tm.api.repository;

import java.util.List;

public interface Repository<T> {

    List<T> findAll();

    T findOne(final String id) throws Exception;

    T persist(final T t);

    T merge(final T t) throws Exception;

    T remove(final String id) throws Exception;

    void removeAll();

    String getIdByNumber(final int number);
}

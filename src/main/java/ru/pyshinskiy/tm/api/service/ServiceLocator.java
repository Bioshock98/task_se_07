package ru.pyshinskiy.tm.api.service;

import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.TerminalService;

import java.util.List;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    TerminalService getTerminalService();

    User getCurrentUser();

    void setCurrentUser(final User user);

    List<AbstractCommand> getCommands();
}

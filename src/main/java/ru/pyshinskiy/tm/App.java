package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.bootstrap.Bootstrap;

public final class App {

    public static void main(String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
